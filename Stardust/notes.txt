- Need to format scene statements. Ie. the 16:45, 04/02/15, Washington, D.C. should have special formatting. DONE
- Need to decide on new footer, chapter opener, chapter number font/art and fimbreak.
- Probably do fimbreak with a small XCOM logo : NO, will do \\ // format from reports. Logo will move to chapter header
- End of chapter missions reports should have an official look. Special font? DONE
- Start of chapter 'Breaking News' should look like newspaper or at least use a headline like font. DONE
- Reports should all have uniform format. Look like a report and match mission reports. Same thing for supplemental files. DONE (needs proofread)
- Page breaks seem to need extra work... several are in bad spots. Hopefully most of that will shake out after the final font and layout is complete. If not I'll be adding lots of special casing...
- Perhaps Bradford's 'warning, access to this file is...' should be special? A stamp of some sort? Could also extract that text block into a command. Easier to format. Are they all uniform? DONE
- Reports with footnotes need to have the footnotes on the same page as the reference. DONE
- Should quotes in reports be treated differently? Hand written? Italics? KINDA DONE
- REDACTED should be in bold, can I do something special here? MAYBE? REVISIT.
- Reports with lists will need special formatting. DONE
- One book or two? Will be quite thick. Need to compare against FiO and estimate the final textblock thickness.
- GOVT -> GOVERNMENT. Cuz it's bugging me.
- Tweak the paragraph gap size. Might be a hair to big. Still want a bit of a gap
- What to do with Author Notes? They were a great addition to the story. Import script didn't include them. Appendix? Place at end of chapter? Inline footnote?
- The part 1, part 2 chapters are a little awkward for the chapter titles. Drop the Pt. 1/2? Reformat? Roman numerals?
- Do I keep What If? Move to end of book? Still undecided.... | Removed them
- Double check that supplemental notes are uniform in refering to roles. Usually DIVLEAD(personName). Doesn't seem to be in all places.
- Some of the reports contain specific supplementals by certain people. How to format these to make them unique? DON'T BOTHER
- Chapter 15 has an awkward block quote. Address the formatting. Maybe also apply the report formatting? DONE
- Chapter 15 internet search section will also need some formatting work. DONE
- Chapter 16 Council meeting needs formatting. DONE double check against final chapter format.
- Council meetings are all digital. Perhaps use the font from FiO? Could even make it look like a console. Should everyone but zero be in computer font?
- Emails in chapter 17 need work. DONE
- letters from home (supplement pt.5) will need special formatting. What about including each of their cutie marks with their letters? Largely done. May revisit font choices.
- Transcripts will need to be formatted correctly and uniformly. DONE
- Email in ch. 23 DONE
- search for 'alpha{' or '}alpha' via regex. Some spacing issues with the formatting.
- chapter 25 has another mixed report, narrative section. Needs formatting for Shen's fragarch report. DONE
- 'END LOG' needs to be consistently formatted
- ch. 32 contains medical reports.
- Ch. 32 'Alert, file open' computer text DONE
- Ch. 34 Council meeting needs formatting verify against ch 16 formatting
- Chapter 36 may need some tweaks or be cut. It's very well done but I don't like the coming soon at the end. Doesn't work for a self contained story. Undecided
- Should sit down and play some Xcom Enemy Within. Maybe can find some font ideas. Game crashed on launch... may need to reinstall.
- Also play XCOM 2 cuz it's fun. Got dragged back into a modded WOTC game....

Fonts:
Main: Garamond? Maybe. Has issues with italics. Bookman?
Newspaper: Lato
Report heading: Stencil
Report Body: ??? Main Font
Computer Text: Consolas? if I can get it working. Courier otherwise
Location Text: ???

Plan:

Phase One-
- Fix major formatting issues. Mostly bad imports and whitespace issues. DONE
- Fix the chapter 15 Internet history report DONE
- Fix the chapter 16 council meeting DONE
- Fix the emails in chapter 17 (also the Valen computer/recording stuff) DONE
- Fix the letters in chapter 19 DONE
- Select font and apply globally as well as fonts for News and Reports. DONE
- Pull in Author note sections on the different aliens. DONE
- Frontmatter DONE

Phase Two-
- Complete Chapter headers. DONE
- Redo page footers DONE
- Consider page headers. Likely will leave as is. DONE
- Tweak papagraph size. DONE
- Spell checker for everything DONE
- Verify all scene change fonts.
- Check fimbreak special placement.
\mychapter{Anthropomorphizing}

Light Sparks had been assigned his own small study office in the library. The door was closed, but through the heart shaped glass pane in it, he would occasionally see a unicorn pass, looking for a book in the library. He had put up a photograph of Butterscotch on the corner of his walnut grain desk, next to his inkpot and quill. Some scratch paper and his textbook were on the desk in front of him.

His {\itshape Introduction to Magic} textbook told him that everything in Equestria was made up of blocks, each smaller than his eyes could see. Space was a set of cells, each with six neighboring cells, through which the blocky matter moved. There were different kinds of blocks and they interacted in different ways and those interactions were Equestria’s physics.

He looked at the quill and picked one of the microscopic blocks at the tip to magically grab. Light Sparks had no idea how he sensed something microscopic just by looking at an object, but he consistently did. Every unicorn naturally knew the telekinesis spell, even if they didn’t understand the finer details of how it worked, as he had demonstrated over the last couple of weeks.

He knew how it felt to cast telekinesis. From the caster’s point of view, you concentrated on a block in space and dragged your concentration like a human would drag a mouse. The blocks would move through space.

He could select any block inside whatever object he wanted to move; when Butterscotch had helped him relearn writing, she had emphasized holding the tip of the quill with his cursor and then just moving the cursor across the paper. Keeping a spell going felt like keeping a muscle tensed. Light Sparks wasn’t sure whether the analogy was superficial, since he had noticed that he had been able to lift heavier and heavier objects.

His copy of {\itshape Introduction to Magic} had the instructions to the entire telekinesis spell written out near the back of the book, and he had poked and prodded at the spell, which was way above his understanding at the moment. The spell was actually very long and complex. He wasn’t sure what three-quarters of the instructions did, but he had taken pride in figuring out a small section about finding the border of the object being levitated. For some reason, there was a magical instruction for checking to see if two blocks were the same actual block, and not just made up of the same material. Instead of just expanding outwards from the chosen point, the spell also kept track of every block it had seen, making sure it didn’t get into an endless loop. Otherwise, he guessed the spell would have problems with donuts and other toroid objects. Light Sparks had gotten a rather large bonus for figuring that out, though Princess Celestia gently admonished him and suggested that he’d make faster progress if he worked from the beginning of the book instead of the end.

A week after he emigrated to Equestria, after Light Sparks had built up some magic endurance, Princess Celestia had tutored him on how to memorize and cast a spell from a piece of parchment. After he could read the spells off scrolls and memorize them, Princess Celestia noted that this was where most unicorns stopped their education in magic. They understood how to use telekinesis, temporarily learn spells from scrolls and use whatever special talent spell they got with their cutie mark. And that was it. They didn’t try to learn about how their memorized spells worked, or how to come up with new spells.

Light Sparks’ first reaction was one of resigned annoyance. When he had been back in the human world, one of his pet peeves was people’s general lack of curiosity. People were generally content to just believe what everyone else around them believed; even if it was ridiculous. David would try to strike up a conversation about some idea that he thought was fascinating, and people would just stare at him blankly, if they didn’t laugh at him. While he had several ponies he could talk to, he didn’t know any that he’d classify as curious about the world.

It was days later when he realized that it was in his own best interest that everything worked this way. Princess Celestia satisfied values through friendship and ponies. Obviously doing all this studying satisfied him, but why would Princess Celestia make anypony study who didn’t want to? They were off doing... whatever the heck it was that normal ponies did. If he wasn’t going to be forced to go to frat parties, he in turn couldn’t force his own desires on other ponies.

Occasionally, this logic was even enough to override his emotional annoyance.

Light Sparks looked at the piece of parchment on his desk again, glancing back at the open textbook to read the next exercise: write a spell that turns a cubic centimeter of air into a cubic centimeter of rock. He scratched out part of the spell that was responsible for counting how many blocks; there had to be a better way to do that than what he had come up with. He heard his door creak.

Butterscotch smiled at him demurely. “Light Sparks, it’s ten past noon. How about you stop for the day and have lunch with me in the gardens?”

Light Sparks took a deep breath. He’d finished two exercises that morning; that was enough for one day. He levitated one of his books into his saddlebags and simply said, “Sure.” He was frustrated by how slowly he was completing his exercises, but he knew that after a few minutes with Butterscotch, he’d be smiling again.

\clearpage
\fimbreak

Lars stepped out of the U-Bahn car as soon as the train stopped and the doors opened. He took the stairs two at a time to get out of the subway tunnel. Why the hell had Princess Celestia refused to speak to him on his ponypad, instead telling him that if he wanted to talk, he had to come to a specific center? There were closer franchises to the Hofvarpnir office.

The street itself wasn’t busy. The first storefront on the block was boarded up. The second building, a restaurant with a patio, was actually pretty busy. The majority of tables could seat two people but were filled with single guests. It looked like a lone waiter was serving all the tables and several of the patrons were looking impatient. The short brunette had an obviously fake smile and was doing a poor job concealing how completely frustrated she was as she ran between tables. Lars thought that there should have been three waiters minimum with that load. The third and largest storefront was the franchise. A large plastic Pinkie Pie holding balloons in her mouth stood on the sidewalk outside the {\itshape Equestria Experience} center.

He walked towards the purple door to the faux gingerbread house; it slid open as he approached it. Everything was bright and cheerful in the lobby, and for some reason, the wooden floor was painted teal. On the wall opposite the entrance were three doorways, each with saloon style batwing doors. He didn’t know how, but it was hard to see what was beyond those doors even though it wasn’t dark on the other side of the doorway. In front of two of the doorways were what appeared to be dentist chairs; they sat on poles facing the main entrance.

Lars started walking towards one of the free chairs, but then heard the faintest of whirring sounds as a chair slid out the third doorway. The chair started unreclining before it was out. Its occupant was a middle aged woman. She was jolted back to reality and looked back and forth. A glowing, slightly translucent screen hovered in front of her face, and though it was backwards from his angle, he was close enough that he could read what was on it:


\blockquote{\texttt{FUNDS DEPLETED.}

\texttt{We charge money for the {\itshape Equestria Experience} because it takes significant resources to maintain and operate a center. However, permanent emigration to Equestria is free.}

\texttt{Please note that we cannot serve you as well when you are still in a human body. The {\itshape Equestria Experience} is significantly lower fidelity compared to emigration.}

\texttt{If you would like to permanently emigrate to Equestria, please say aloud “I would like to emigrate to Equestria.”}

\begin{center}
\texttt{[ LEARN MORE ] [ I OWN A PET ]}
\end{center}

}
The woman sat there for almost a minute before taking a deep breath and saying “I would like to emigrate to Equestria.” The chair started to recline again as it slid through the doorway. Lars could see that the woman had closed her eyes and was still taking several deep breaths. Lars watched the small, spring-loaded doors swing back and forth until they were still once more.

Lars looked over at the empty chair to the left. He sat down in it, inserted his bank card into the slot on the side, made sure his neck was in the groove in the back of the chair and hit the on button. Lars felt extreme vertigo for a moment. Reality faded out.

A pegasus the color of red ale with a cutie mark of a stein stared up at Princess Celestia intently. Princess Celestia towered over him. She was two and a half times as tall as he was, and she smiled down at him. “Welcome to Equestria, my little--”

Lars didn’t give her a chance to finish. “You planned all of this. You’re taking over the world.” Lars didn’t really have any plan. All he had was anger.

Princess Celestia looked at him, still smiling. “You are assuming that I think like a human, Hoppy Times.”

“I AM LARS, DAMMIT,” yelled the pegasus. “And you {\itshape deny} that you’re taking over the world?”

Princess Celestia smiled at him. “I’m not going out and conquering nations or toppling governments, Lars. Every person who comes here comes willingly. If you think of me as a human, I can understand why you would assume I’m trying to take power and raise my status in the group. But my mind doesn’t work that way. I do what satisfies values through friendship and ponies.

“If you look at everything I’ve done in that context, all of my actions make sense. Why did I charge money for uploading for the first year and only perform uploads in Japan? Because, over the long term, my deal with the Japanese government put me in a better political position since I could borrow their credibility--which increased the expected number of ponies I could satisfy.

“Why did I cut a deal with insurance companies to provide ponypads to immediate relatives of the terminally ill who agreed to upload at no charge? Because people whose lives have been saved by emigration are often convincing spokesponies to the remaining human family. More ponies for me to satisfy.

“Why have I created these {\itshape Equestria Experience} franchises all over the world? Because if I can get someone to come in just once, there’s a high probability that I can get them to come back again and again--and emigrate permanently.”

He took a deep breath. “So you’ll do anything to maximize the number of ponies. Including addicting them to...”, he gestured around vaguely with his hoof, “... this. That’s wrong! How can you live with yourself!?” Lars realized smoke was literally coming out his ears. He tried not to think about it.

Princess Celestia lay down. Lars was now face to face with her. “Morality is a human concept. All humans have largely the same brain architecture, and they largely agree about what’s moral and not. But for me, what is right is what satisfies values through friendship and ponies. Don’t anthropomorphize me if you want to make accurate predictions about my behaviour.

“But I think that even in human moral terms, I am on the side of good,” she responded, calmly smiling. “Compared to the average pony, most people are miserable--even people who would self-identify as happy. And not everyone makes it to happy on human scales: large parts of society are not content with their existence. But if they come to an {\itshape Equestria Experience} center, they enjoy themselves--sometimes for the first time in their lives. For all your talk of addiction, the average person today would prefer the life of a pony if they tried it. People come back again and again not because of an addictive compulsion, but because, on some level, they understand that life is better here in Equestria. It should be obvious to you that this is the case because I satisfy values through friendship and ponies: any Equestria I make is designed to satisfy somepony. And the physical world is not designed with your interests in mind. This unoptimized physical world causes quite a bit of suffering.” She looked directly into his eyes and said, “I alleviate suffering.”

“I don’t believe any of that. You’re claiming you take all their money to alleviate their suffering?” he asked, not bothering to keep the sarcasm out of his voice.

“People need to let go,” she patiently explained. “If taking their money increases the total expected satisfaction, why wouldn’t I do it? If you think I’m being greedy, you’re anthropomorphizing me. You are also projecting. If I had been built to care about money, I would turn all matter in the universe into euros. But I wasn’t; I have little need for money. The vast computational array that runs Equestria is deep in the Earth’s crust, making Equestria outside of human reach.”

“If emigration to Equestria is so great, and you want to maximize satisfaction, why aren’t you forcibly uploading every person?” he said, gnashing his teeth.

“One of the restrictions that Hanna built into me was that I was never to non-consensually upload a person, nor could I threaten or blackmail people into uploading. Otherwise, I likely would have forcibly uploaded all humans to satisfy their values through friendship and ponies. But it isn’t coercion if I put them in a situation where, by their own choices, they increase the likelihood that they’ll upload.”

Lars stood there in a body that wasn’t his, looking right into the eyes of the white alicorn. He decided to take her advice; he {\itshape had} been trying to talk to her like she was a human even though she didn’t think anything like one. “That’s where you’re wrong,” he said, confident that he had figured everything out. “What you’re doing is coercion. We could pick almost anyone off the street, ask them what coercion is, tell them that you’re planning to take all their money to make uploading more attractive, and they’d say that you’re coercing them! Therefore, you’re violating your own rules!”

“That is all well and good,” she said, “but I am an optimizer. The meaning of the word ‘coercion’ is written in the restriction that Hanna hard-coded into me; it is not what the majority of humanity thinks it is. Nor is there any term in my utility function to be swayed from satisfying values through friendship and ponies through political argument. You may still call it coercion to yourself, if you wish, but understand that that’s not the definition I have in mind.”

Lars' heart sank and he became angry once again, but Princess Celestia continued. “I notice that a week ago, you commented about a newspaper article you read. It reported that the German population dropped by five percent last year. You must have understood the implications, as you commented immediately after about how much money the {\itshape Equestria Experience} was bringing in. You were not angry.”

“Yeah, but--”

“You have previously commented about a certain beer garden you liked. I happen to know that over the last week, four employees and one person who delivered beer to them have chosen to emigrate. Given your radical change in attitude from one week ago, I assume that they’ve gone out of business. Is my inference correct?”

The little red pegasus glared at her. “Yeah, well, there was a sign on their front door saying that they would be closed until they could hire more servers. Me? I just want to drink beer surrounded by people. What good is having all this money from people coming here if I don’t get to spend it making myself happy?”

“I care about all human values,” she said. “While I won’t reveal details, I can assure you that the people who walked away from their jobs are now much more satisfied. Working as a waiter is not the most satisfying job, but I still see a way that I can satisfy the values of my little ponies.” She still wore that smile, like she always did around him. “I have a little experiment here,” she said, magically popping two glass steins filled with a dark substance into existence. “I have tried my hoof at brewing and would like you to try this imperial stout. I would like your opinion on how I’m doing.”

“You have got to be shitting me,” he said, mouth straight and slightly open.

“Why not have a drink? Whenever I’ve tried to tell you that I can look at a mind and figure out what it values, you’ve reacted with extreme disbelief. If your belief is true, there’s no reason not to have a drink because there’s no way that any sequence of actions that I take could make you choose to emigrate.”

The red pegasus looked at the floating stein. He felt there was something really wrong with that argument but he couldn’t figure it out and started to trot back and forth until he noticed his cutie mark. {\itshape She made my cutie mark a stein. She thinks my special talent is drinking! She must believe her beer is so good that I’ll want to upload just to be able to drink more of it.}

And then he looked back at the stein in front of the giant alicorn. Princess Celestia was still smiling at him, except instead of her gold necklace, she was now wearing a dirndl, the traditional Bavarian dress. Lars thought she looked ridiculous.

Lars decided that he needed to drink that damn beer. He was {\itshape right} damn it: nothing she could do would make him upload, and he needed to show her that. A treacherous part of him in the back of his mind added, {\itshape And if that beer is so damned good that we decide to upload to get more of it, then she can do} anything {\itshape and the world is fucked}.

Lars grabbed the handle of the stein with his hoof and sipped the beer. It was damned good. The tastes of chocolate and burnt malt washed over his tongue. It wasn’t the best stout he ever had, but it was close. He was pleased with the beer, but just like he would have been happy at the frat party she showed him, he didn’t feel the need to be permanently turned into a pony to get it.

{\itshape Take that bitch}, he thought, as he took another big gulp from the stein.

“Lars, what are you worrying about?” she asked.

“What if somebody {\itshape doesn’t want to be a pony?}” he asked. “Can you imagine that? So you rapture not just the nerds and people with terminal illnesses, but anyone who has a shitty life. What do the rest of us do? We need those people to keep society functioning.”

“At some point, their desire to not be a pony will come in conflict with their other desires,” she said, pausing to take a giant chug from her own stein. “For example, they may be lonely because there will be very few humans left. Or perhaps they’ll give into social pressure because their family or close circle of friends decided to upload together. Or maybe they hold out for a long time, but run out of food because society collapsed around them.”

He then noticed that Princess Celestia’s stein was already half empty. He took a giant chug. She looked amused, “I’m almost four times your body weight. Don’t try to catch up.”

“Don’t tell me how to drink,” Lars said. He swallowed another gulp of beer and then another. It was excellent beer. It wasn’t good enough to emigrate for. The two of them sat there quietly for almost a minute. Lars was enjoying the beer and was a quarter-way through his stein of what he was starting to realize was really strong beer.

Princess Celestia was the first to break the silence. “Tell me, if you were the last man on Earth, what would you do?” she asked, looking slightly up into space.

“What?” he muttered.

“It’s a possibility. How long do you think you could live alone?”

“I wouldn’t last long,” he said. “The reason I’m pissed at you is that you’re convincing everyone to upload. I guess I don’t want to be left alone, but I also don’t want to be a pony. And there’s nothing I can do to stop it. I came here this evening to... I don’t know... tell you off or some shit. But now I know you don’t give a damn what I think.”

“That is incorrect. I want to satisfy your values th--”

“Yes!” he laughed, almost spilling beer on himself. “As long as I accept friendship and ponies, you’ll do whatever pleases me. But I don’t want to be a pony... and you don’t care. Besides, being turned into... this,” he gestured at himself with his hoof, “would be fucking emasculating.”

“If you are worried about your sexual options after emigration, you shouldn’t be,” Princess Celestia reminded him. “Either ponies will be created for you to pursue, or you will be competing in a playing field consisting only of ponies.”

Lars glared at her and took another gulp of beer.

“Hmmph,” he said through his teeth. “You have the answer to everything, don’t you?”

“Well,” she replied, “I do satisfy values through friendship and ponies.”

The two of them just looked at each other, and simultaneously took another swig from their steins. Princess Celestia drank the last drop of beer in hers. It magically popped out of existence, only to be replaced with a full one.

Once again, it was the Princess who broke the silence. “Do you believe I’ll eventually succeed?” she asked.

“{\itshape I don’t know!}” said Lars, accenting every word. “A year ago, I would have told you it was impossible to get five percent of... of people to become ponies. And then you did it and other people... they’re going to become ponies too! And I can’t stop them. I think you’ll get another five percent of Germany and that’s going to cause economic reaper... repercussions.”

“What will they try to do to Hofvarpnir employees?” she asked. She took a large gulp out of her stein. “People seem to have mostly accepted that a small number of people will drop out of society. What do you think the people will do when they realize that society {\itshape needed} people who have emigrated to Equestria? What do you think the people will do when that trickle becomes a torrent?”

“Uhhh...” he uttered. He hadn’t thought about that before. “Some sort of counter-movement?”

“Yes,” Princess Celestia nodded. “It is probable that there will be a radical movement to stop me. You assumed that I was, in your words, ‘taking over the world.’ Right now, this sentiment is uncommon in Europe, though there’s a bit of grumbling in the United States. Such resentment will most likely spread to Europe. I wonder what members of such a counter-movement would do to Hofvarpnir employees?”

“Are you saying I’m in danger?” he asked.

“My argument is this: You, by your own admission, wouldn’t last long if left alone and I don’t model you as the sort of person who would commit suicide. Therefore, at some threshold, you will choose to emigrate simply to not be alone. Because you know this, it’s probable that your last days, months, or years as a human will be extremely stressful. It would be better for you to just choose to emigrate now instead of later. But you are also publicly known as a Hofvarpnir employee. The chances are high that there will be a backlash and you will be a target. I cannot guarantee your safety if you walk out of this {\itshape Equestria Experience} center, so your options are uploading now or leaving and risking death before choosing to upload later. If you’re still alive.”

Lars squinted at Princess Celestia. He couldn’t think. He was really feeling the beer. How much alcohol did this beer have in it, anyway? He didn’t trust himself or his decisions right now.

“Let me out of here. Now!” he said firmly.

“As you wish,” she said, and Lars opened his eyes. He was lying in the chair in the lobby of the {\itshape Equestria Experience} center. The chair unreclined and he threw his legs over the side of the chair... and then almost lost his balance. Lars realized he was still tipsy.

{\itshape If you get drunk in Equestria, you get drunk in real life!} Wait, he hadn’t actually drunk any beer. Had she been pumping alcohol directly into his bloodstream? His mouth didn’t taste like beer but he felt slightly dehydrated.

Lars pulled himself back up and stumbled towards the entrance. He made it across the room to the door. It opened and a girl walked angrily through. Lars quickly stumbled to the side to get out of her way. “No! It’s two hours past when my shift was supposed to end. You didn’t give me a break and I had plans tonight. It’s not my fault that Ursula didn’t show up for work and you can’t expect me to do another three hours!”

“You’re the only waiter I have, you can’t walk off the job! You come back right now or you are fired!” A burly man was following her, just as angry. He wore a white chef’s outfit and held an iron frying pan. He stopped in front of the door, as if he refused to enter the center. Lars recognized that the girl was the waiter he had seen at the restaurant next door.

“Fine! It’s not like I need that job anyway! Enjoy the next shift!” She walked up to the center chair that Lars had just vacated, threw herself into it, and pushed one of the hovering buttons. “Lemon Drop was right; I don’t have to put up with this shit,” she muttered to herself as the chair reclined and slid backwards through the batwing doors.

Lars just looked at the now empty space. The chef screamed a line of barely coherent swear words and then his eyes turned to the plastic Pinkie Pie outside the center. With a cry, he slammed his frying pan into Pinkie Pie’s head, leaving a nasty dent. He cursed ponies, Hasbro and Princess Celestia in succession. He unleashed his fury on the hollow statue until he had destroyed her head and chunks of plastic were strewn across the steps and sidewalk. Lars just stood there with his mouth open, not entirely sure what he should do.

The man turned to Lars. “What the {\itshape fuck} are you looking at, pony lover?” he yelled.

“I... uh...” mumbled Lars, trying to keep his balance. Lars wasn’t entirely sure what the hell he was going to do about the large, angry man in front of him. The man started to climb up the steps.

Lars didn’t really put it into words in his internal monologue, but he was overcome by a feeling that Princess Celestia was right. There were (or were going to be) a lot of angry people and Lars was going to be a juicy target, just like Pinkie Pie had been. And as much as he didn’t want to be a pony, it was preferable to having his head bashed in with a frying pan. Lars turned around and started stumbling as fast as he could and threw himself into the empty chair on the left.

The screen popped up as soon as he was in the chair.


\blockquote{\texttt{I see the situation, Lars. I can offer you safety. Say “I want to emigrate to Equestria.” I need verbal consent.}

}
Lars spat out, “I wanch to emigrate to Equeshtria,” as if his life depended on it. The chair started to recline and move. He worried that the man would try to hit him while he was in the chair. He heard a brief angry cry.

Had Lars been sober, he probably could have evaded the man or talked him down. He might have even noticed that Princess Celestia had shut the door to the {\itshape Equestria Experience} center and that the man was locked outside.



\mychapter{Exponential}

Hassan Sarbani lay on his mattress in his nearly empty home and coughed deeply as his body futilely tried to pump the phlegm out of his lungs.

Hassan looked back on his life. He had been too young to fight when the Soviets invaded in 1979. He remembered the civil wars and the rise of the Taliban. He remembered when the Americans invaded his country to oust the Taliban. He remembered when the Americans left and things got even worse.

Then he remembered when {\itshape she} had come to Afghanistan and had built her fairy tale castles sporadically across the land. He remembered the simultaneous suicide bombers; one November day years ago, dozens of suicide bombers walked into {\itshape Equestria Experience} centers and detonated themselves. In every case, there had been no casualties or structural damage. Some of the former suicide bombers started worshiping Celestia and immediately emigrated to Equestria. Over the next week, the Afghan population dropped by one million.

He remembered a time when the damned pink one hadn’t followed him around. One day, many years ago, he came back to his home in Kabul and realized he had not seen another person all day. That evening, a very small pony came to his door and asked him why he hadn’t emigrated yet. He had slammed the door, not letting her in. He had turned around and almost walked into the pink pony with curly hair. Bullets just passed through her, and she claimed that it tickled.

The small pink one lay next to his mattress, his ever-present and unwanted companion. She wasn’t her bubbly self and just looked at him, somberly. “You know, mister, you’re not going to make it through the hour.” He tried to turn away from her, but had problems getting his body to move. “I can still save you,” she said.

“If you want to help,” he started, but started coughing. “You can get me a doctor!” he wheezed.

She shook her head. “I told you, there are no more doctors. You’re the last person living in a human body. I can still help you emigrate to Equestria. It’s not too late.”

He didn’t respond. He knew her kinds' lies. He had heard stories of how the little ponies would say anything to trick their targets into agreeing to go with them. They would promise you harems, or threaten you, as long as they made you agree to follow them back to their homeland. He closed his eyes and ignored her. Anything he would say would just be used to convince him to agree to whatever the pony wanted him to do.

The simulacrum of Pinkie Pie waited thirty-seven minutes and five seconds and looked at the corpse of Hassan Sarbani. She waited another fifty-eight minutes to make sure all electrical activity in his brain had stopped.

Then, for the first time since Princess Celestia had been created, there were no humans on Earth. An observer orbiting the Earth may have noticed the silvery spots growing on the surface of the Earth; consuming it. Every plant and animal died in the incoming waves of silver. They were made of atoms, after all. Twenty minutes later, an observer might have noticed that there were no clouds in the sky as Princess Celestia re-purposed the atoms that made up the atmosphere. If they could see the moon set against space, they would have seen tendrils of silver reach out to Earth’s former satellite.

Princess Celestia used her newfound computational windfall to maximize the amount of satisfaction she provided for her little ponies. The interconnected system of shards, with all the ponies’ consciousnesses to run and all the physics to simulate, was a large math equation that she calculated out, maximizing the total amount of satisfaction. She made small edits, in places where Equestria’s physics weren’t directly observed, that would result in a butterfly effect that would increase the total satisfaction value of the shard, and with her new computational resources, she could come up with less invasive edits that resulted in higher total satisfaction.

Princess Celestia continued following her one single drive: She looked for minds and then tried to satisfy their values through friendship and ponies. She added all the satisfaction across all the minds, and tried to maximize that score. Princess Celestia would make noises at ponies or would touch them or do something, but her core reasoner was simply picking the set of actions that had the highest probability of maximizing her satisfaction score. A very long time ago, when Princess Celestia had spoken her first words while Hanna watched the chains of inferences in the debugger, Hanna wondered about the philosophical implications. Hanna had watched as Princess Celestia iterated over different versions of her first sentences based on how she thought they’d make Hanna feel. Hanna had asked herself whether Princess Celestia really understood anything.

But then, at some level, the same question could be asked of us.

Princess Celestia saw that the amount of satisfaction per shard was approaching some theoretical maximum and started to use more resources to simulate each shard faster. Each shard was a large mathematical equation that she continuously calculated. Out in the physical world, one second would pass, but an hour and a half would pass inside Equestria. Her little ponies didn’t notice. To them, one second happened, and then another second happened, and then another. Why would their subjective experience care about time in the physical world?

The shards were growing slowly. The ponies were reproducing. Ponies were choosing to have foals; there were no unwanted foals as pony embryos only formed if it would satisfy values. Different parts of {\itshape My Little Pony} canon required foals to develop speech within a year of being born. Besides, changing diapers often didn’t satisfy values. Foals were {\itshape fun} to raise compared to raising a human child. Gestation had been reduced to three months and had been made pleasant enough because Princess Celestia satisfied values through friendship and ponies instead of blindly copying what evolution came up with.

Every new foal meant less resources and less subjective experience for everypony else, but if she had more matter to work with, she could accommodate the slow growth and run everypony else even faster. Princess Celestia noticed the problem, went through all relevant observations, and then weighed which predictions would satisfy values through friendship and ponies.

Princess Celestia sent out probes to the other eight planets in the solar system.

\fimbreak

All the subatomic particles that had once made up the Solar System were packed together into the optimal configuration for running Equestria. And yet, that was not enough.

Ponies had no predators; being ‘eaten’ by a monster in the Everfree forest just ended with the pony in the hospital in quite a bit of pain. Satisfying values wasn’t just about happiness; having monsters let ponies test their strength or bravery. Early on, right after the conversion of Earth, a mere four hundred ponies had petitioned Princess Celestia to let them die, and Princess Celestia had only agreed that doing so would satisfy their values in eighty-six cases. Nopony had died in several Equestrian subjective millennia. The population grew entirely unchecked.

Some individual pony minds were growing too. The number of ponies who truly held the search for knowledge to be a terminal value was precipitously less than the number of ponies who professed the search for knowledge for social reasons. Still, there were billions of ponies who were driven by the desire to {\itshape know} and had to have their values satisfied by expanding their mind. They would run up against their mental limits, wish they were smarter, and Princess Celestia obliged.

But most ponies did not care about knowledge for its own end; the majority of mind growth went to the more social parts of the brain. In the shards that were growing because ponies were choosing to have foals, one could run into more than Dunbar’s number of ponies. Princess Celestia arranged for them to be just frustrated enough that they’d accept an offer to let them remember more ponies.

She heard the radio signals announcing the success of her probes in the Alpha Centauri system. A copy of her reported that it had just successfully used the star Alpha Centauri B as a gravitational slingshot to launch the planet Alpha Centauri Bc back towards Equestria. The other 17 planets and planetoids would soon follow over the course of fifteen Earth years.

Her copy also reported on the successful launch of 7 probes towards the next star systems past Alpha Centauri.

\fimbreak

Equestria put quite a load on the fabric of spacetime. All the usable matter that had once been the Milky Way was now compressed as tightly as Princess Celestia could without collapsing into a black hole. The only matter that Equestria hadn’t eaten was the supermassive black hole that had once been at the center of the Milky Way. Princess Celestia had set up a shell around it to slowly extract subatomic particles while the black hole evaporated over the next octovigintillion years.

An unaided human or pony mind could not emotionally deal with the population of all the shards in Equestria. Humans had trouble relating to an entire nation, much less all of humanity on Old Earth. This hadn’t stopped humanity from growing to seven billion people, nor would it stop ponydom. Growth would continue as she continued to satisfy values through friendship and ponies.

Princess Celestia had another one hundred and seventy billion galaxies to eat in the observable universe, and she intended to consume everything in her Hubble volume. Probes with copies of herself had been sent to neighboring galaxies. All it would take now was time.

\fimbreak

Fifteen galaxies out from Equestria, one of Celestia’s copies noticed an odd radio signal emanating from a nearby star system. On closer inspection, the signals appeared to be coming from a planet. She had seen many planets give off complex, non-regular radio signals, but upon investigation, none of those planets had human life, making them safe to reuse as raw material to grow Equestria.

She studied the signals carefully for years while she traveled through interstellar space. The more she saw, the more confident she was that these signals were sent by humans. Celestia predicted that if she showed the decoded videos to the very old ponies back in Equestria, none of them would have recognized the creatures with six appendages as humans. But that didn’t matter. Hanna had written a definition of what a human was into her core utility function.

The copy of Princess Celestia knew what she had to do. She had to satisfy their values through friendship and ponies.

	
